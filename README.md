# Workshop: New ideas in detecting long-lived particles at the LHC
### Berkeley, 10 Jul - 13 Jul 2018

## Introduction

The workshops aims to bring together experimentalists and theorists to brainstorm about new ideas about triggers and off-line analysis strategies for exotic phenomena at the LHC, in particular long-lived particles. The primary goal is to generate new ideas and verify their feasibility, rather than to present existing work.

This repository contains a collection of possibly useful tools and can be used to share code among participants (see instructions below).

## Instructions

We suggest to use this repository to share code among participants of each project.
To do this effectively, the easiest is to create a branch for your own work (forking the repository is another option, if you prefer)

You can clone the repository with:

```
git clone git@gitlab.com:simonknapen/LLP_workshop_LBNL.git 
git clone https://gitlab.com/simonknapen/LLP_workshop_LBNL.git

```

If you want to also pull the standard libraries that are linked to the repository (e.g. HepMC library) just do instead:
```
git clone --recursive git@gitlab.com:simonknapen/LLP_workshop_LBNL.git 
```

You can then create your own branch as:
```
git checkout -b dev-project-X
```

and each participant can get a copy of the branch with
```
git clone --recursive git@gitlab.com:simonknapen/LLP_workshop_LBNL.git
git checkout dev-project-X
```

## Simulation samples

Follows a list of simulation samples prepared for this workshop, together with a contact person for each. 

#### Disappearing tracks
Contact: Laura Jeanty
Description: will come

## Specific instructions on utilities

For convenience we collect here basic setup instructions for each utility. For some of them, see also the README files within their folder.

### HepMC library

Added as a submodule of this repository, currently pointing to version 2.06.09.

https://gitlab.cern.ch/hepmc/HepMC/tree/HEPMC_02_06_09

To build it, assuming you have all the needed tools in your system, just do (from the main repository path, i.e. the LLP_workshop_LBNL/ folder):
```
cd LLP_workshop_LBNL/
mkdir -v build/
mkdir -v build_HepMC
cd build_HepMC
cmake -Dmomentum:STRING=GEV -Dlength:STRING=MM -DCMAKE_INSTALL_PREFIX=../build/ ../HepMC
make
make install
```

Then make sure to include the directory `build/` in others makefiles as appropriate (the ones in this repo are already setup accordingly).

Alternatively, fastjet is already built for common platforms and distributed through cvmfs:
`/cvmfs/sft.cern.ch/lcg/external/HepMC/2.06.08/`

### FastJet

For convenience, we've added the fastjet library to this repository, you can find more information on their website:


To compile it locally, the following should just do it:
```
cd LLP_workshop_LBNL/
mkdir -v build/
cd fastjet-3.3.1
./configure --enable-allplugins --prefix=${PWD}/../build/ 
make
make install
```

Alternatively, fastjet is already built for common platforms and distributed through cvmfs:
`/cvmfs/sft.cern.ch/lcg/external/fastjet/3.0.3/`

### lhef2hepmc

This small utility converts an LHE to and HEPMC file. It also shows the basic on how to read HEPMC files.

The utility needs the HEPMC library; if you have it installed in your system already just edit `lhef2hepmc/Makefile` and add the path there. 
If you have not, you can use the one that is linked to from this repository, just clone the repository including the `--recursive` option, e.g.
```
`git clone --recursive git@gitlab.com:simonknapen/LLP_workshop_LBNL.git`
```
see above for instructions on how to build it.

To use it, make sure the path where HepMC library is installed is available, e.g. from the main repository path (i.e. the LLP_workshop_LBNL/ folder):
```
export LD_LIBRARY_PATH=${PWD}/build/lib/:${LD_LIBRARY_PATH}
```
if you use the repository setup, a convenience bash script is also provided:
```
cd LLP_workshop_LBNL/
source scripts/setup_all.sh
```

### lhefilter

Small utility to filter LHE files.

Requires BOOST (program-options) library, ROOT, gsl installed in your system.

To comile simply:
```
cd lhefilter
make
```

### RivetMCAna

Just one example of a rivet analysis, this one was for a VBS ssWW case, which illustrated dressed leptons, overlap removal, jets, met, cut-flow and histogramming.

To get this working you need rivet installed in your system.
Please also ensure your environment is setup proerly to include the HepMC and FastJet libraries, e.g.
```
source scripts/setup_all.sh
```

Then the following provides a guideline, from the LLP_workshop_LBNL/RivetMCAna/ folder:
* edit/rename MC_VBSVAL.info with general characteristic of your analysis
* edit/rename MC_VBSVAL.cc with the code for your analysis
* edit/rename MC_VBSVAL.plot with the list of plots your analysis will produce and their titles/labels
* `rivet-buildplugin RivetMC_VBSVAL.so MC_VBSVAL.cc -I ../build/include/` ## Note the extra compiler flag to tell rivet where HepMC is installed (see instructions above)
* setup environment: `export RIVET_ANALYSIS_PATH=${PWD}:${RIVET_ANALYSIS_PATH}`
* `rivet -a MC_VBSVAL <hepmcfile>`
* you can use `rivet-mkhtml` to make HTML pages from your output AIDA files, or the rivet `aida2root` utility to convert them into ROOT files.

Alternatively, you can use rivet directly from a machine mounting cvmfs, with:
```
source /cvmfs/sft.cern.ch/lcg/external/MCGenerators/rivet/1.8.1/x86_64-slc5-gcc43-opt/rivetenv.sh
rivet-buildplugin RivetMC_VBSVAL.so MC_VBSVAL.cc -m64 -D__USE_XOPEN2K8
```
he last line is needed because of a change in glibc version on slc6.
To then run rivet inside atlas software open a *new clean shell* and setup atlas.

For more information, see https://rivet.hepforge.org/trac/wiki/WritingAnAnalysis

### RivetDT

A basic Rivet analysis that runs on the chargino samples to check very basic distributions.

Needs Rivet 1.9 (or at least Rivet < 2.0 built on HEPMC 2.06)

To run on lxplus:

setup
```
source /afs/cern.ch/sw/lcg/external/MCGenerators_lcgcmt67c/rivet/1.9.0/x86_64-slc6-gcc47-opt/rivetenv-genser.sh
```

compile
```
rivet-buildplugin RivetDTVal.so DTVal.cc
```

make sure Rivet finds local analysis
```
export RIVET_ANALYSIS_PATH=${PWD}:${RIVET_ANALYSIS_PATH}
```

run
```
rivet -a DTVal /afs/cern.ch/user/l/ljeanty/public/tag_1_pythia8_events.hepmc/
```

make plots
```
rivet-mkhtml Rivet.aida --pdf
```

### hepmc parser

This script parses a HEPMC file, performs an operation on certain particles and writes out a new HEPMC file.
In the example, the script runs Yuri's CMS track trigger script on all electrons and muons, and stores the result by modifying the status code of the particle.
An example in and output hepmc file is also provided. The use the script, modify the path's in the Makefile to link to your hepmc installation and
```
cd hepmc_parser
make
```

for linux system, you can just do:
```
g++ -std=c++11 -I../HepMC/ -L../build_HepMC/lib/ -o hepmc_parser hepmc_parser.cc -lHepMC
```
if you use the built-in HepMC installation compiled and installed as above.

### inelastic dark matter pythia8 script

Example script to set up the event generation of a long-lived particle in pythia 8, and write the output in a reduced hepmc format. The to simplify the hepmc event record, the event is stripped down to its 
bare minimum: LLP, mother and sisters of LLP and daughters of the LLP. The example that is implemented is that of inelastic DM, as in 1508.03050. The process is 
```
p p -> A' j
A' -> chi1 chi2
chi2 -> chi1 mu+ mu-
```
where chi2 is the LLP. To run the example:
copy inelastic_DM.cc to the example folder of your pythia8 installation. 
add 'inelastic_DM' to the line in Makefile which links the HEPMC2 libraries (the one with 'main41', 'main42' etc)
```
cd pythia8xxx/examples
make inelastic_DM
```
the hepmc libraries must be linked in your pythia8 configuration file. 


