#include <iostream>
#include <fstream>

//#include <HepMC/IO_HEPEVT.h>
#include <HepMC/IO_GenEvent.h>
//#include <HepMC/IO_AsciiParticles.h>
#include <HepMC/GenEvent.h>

#include <fastjet/ClusterSequence.hh>

#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TLorentzVector.h>

int main(int argc, char* argv[])
{
  //
  // Prepare input
  std::ifstream istr("/afs/cern.ch/user/l/ljeanty/public/tag_1_pythia8_events.hepmc");
  if( !istr )
    {
      std::cerr << "example_ReadMyPythia: cannot open example_MyPythia.dat" << std::endl;
      return 1;
    }
  HepMC::IO_GenEvent ascii_in(istr);

  //
  // Prepare output
  TFile *fh_out=TFile::Open("output.root", "RECREATE");
  TTree *t=new TTree("tree", "tree");

  // branches
  TClonesArray *ar_charginos = new TClonesArray("TLorentzVector");
  t->Branch("charginos", &ar_charginos, 32000,10);

  TClonesArray *ar_jets = new TClonesArray("TLorentzVector");
  t->Branch("jets", &ar_jets, 32000,10);

  double weight;
  t->Branch("weight", &weight, "weight/D");

  //
  // Prepare fastjet
  std::vector<fastjet::PseudoJet> particles;

  // choose a jet definition
  double R = 0.4;
  fastjet::JetDefinition jet_def(fastjet::antikt_algorithm, R);
  std::cout << "Clustering with " << jet_def.description() << std::endl;

  //
  // now read the file
  int icount=0;
    
  HepMC::GenEvent *evt=0;
  while ( (evt = ascii_in.read_next_event()) ) // && icount<100) 
      {
	ar_charginos->Clear();
	ar_jets     ->Clear();

	icount++;
	if ( icount%50==1 ) std::cout << "Processing Event Number " << icount
				      << " its # " << evt->event_number() 
				      << std::endl;

	// weights
	weight=evt->cross_section()->cross_section()/evt->event_number();

	// Loop over particles
	particles.clear();
	for ( HepMC::GenEvent::particle_const_iterator p = evt->particles_begin(); p != evt->particles_end(); ++p )
	  {
	    if((*p)->status()!=1) continue; // Only care about stable particles
	    // Add the chargino
	    if(abs((*p)->pdg_id())==1000024)
	      { // Chargino!
		TLorentzVector *mom = (TLorentzVector*)ar_charginos->ConstructedAt(ar_charginos->GetEntries());

		mom->SetPtEtaPhiM((*p)->momentum().perp(),
				  (*p)->momentum().eta(),
				  (*p)->momentum().phi(),
				  (*p)->momentum().m());
	      }

	    // Prepare the jet collection
	    // No charginos, neutrinos or muons
	    if(abs((*p)->pdg_id())!=1000024 && // chargino
	       abs((*p)->pdg_id())!=12 && // nue
	       abs((*p)->pdg_id())!=14 && // numu
	       abs((*p)->pdg_id())!=16 && // nutau
	       abs((*p)->pdg_id())!=13) //mu
	      {
		// px py pz e
		particles.push_back(fastjet::PseudoJet( (*p)->momentum().px(), (*p)->momentum().py(), (*p)->momentum().pz(), (*p)->momentum().e()));
	      }
	  }
	// run the clustering, extract the jets
	fastjet::ClusterSequence cs(particles, jet_def);
	std::vector<fastjet::PseudoJet> jets = fastjet::sorted_by_pt(cs.inclusive_jets());

	for(const fastjet::PseudoJet& jet : jets)
	  {
	    if(jet.pt()<20) continue; // 20 GeV filter
	    TLorentzVector *mom = (TLorentzVector*)ar_jets->ConstructedAt(ar_jets->GetEntries());

	    mom->SetPxPyPzE(jet.px(),
			    jet.py(),
			    jet.pz(),
			    jet.E());
	  }

	t->Fill();
      }
    //........................................PRINT RESULT
    std::cout << icount << " events found. Finished." << std::endl;

    t->Write();
    fh_out->Close();
}
