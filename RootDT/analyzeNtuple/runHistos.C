#include "charginoNtuple.C"

#include <iostream>

using namespace std;

void runHistos() {

  string inputLoc = "./";
  string outputLoc = "histos/";

  vector< pair <string, string > > samples;
  samples.push_back( make_pair("charginopair_200GeV.root","200_GeV"));
  samples.push_back( make_pair("charginopair_500GeV.root","500_GeV"));
  
  for (uint i=0; i<samples.size(); i++) {

    string filename = inputLoc+samples[i].first;
  
    TFile * f = TFile::Open(filename.c_str());
    if (!f) {
      cout << "Cannot open file: " << f << endl;
      return ;
    }

    TTree *t = (TTree*)f->Get("tree");
    if (!t) {
      cout << "Cannot find tree in file: " << f << endl;
      return ;
    }

    charginoNtuple * runner = new charginoNtuple(t);
    runner->InitHistos();
    runner->Loop();
    runner->writeHistos(outputLoc+"hist_"+samples[i].second+".root");

    delete runner;

  } // loop over samples

}
