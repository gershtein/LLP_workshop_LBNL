#include <math.h>
#include <stdio.h>
#include <assert.h>

class trigstubs {

 public:

  double prop(double phi, double R, double xD, double yD, double rho, bool &isok) {
    
    double xD0 = xD*cos(phi) + yD*sin(phi);
    double yD0 = yD*cos(phi) - xD*sin(phi);

    double x1  = xD0;
    double y1  = R + yD0;
    double d2  = x1*x1 + y1*y1;
    double d   = sqrt(d2);
    
    double phi_c = - atan(x1 / y1);
    int s = R>0? 1 : -1;
    double y2  = (rho*rho + d2 - R*R)/(2*d)*s;
    
    double phi2 = y2 / rho;
    if(fabs(phi2)>1){
      isok = false;
      return 0;
    }
    phi2 = asin(phi2);
    
    isok = true;
    return  phi + phi2 + phi_c;
  }

  void initialize(){
    printf("initializing trigstubs...\n");
    bool isok;
    double R2 = 100 * pt_cut / (0.3 * 3.8);
    for(int l=0; l<6; ++l){
      double phi12 = prop(1., R2, 0, 0, Rlayer[l],isok);
      assert(isok);
      double phi22 = prop(1., R2, 0, 0, Rlayer[l]+DRlayer[l],isok);
      assert(isok);
      phi22 = acos(cos(phi22 - phi12));
      bend_cut[l] = fabs(phi22 * Rlayer[l]);
    }
    is_initialized = true;
  }

  unsigned int stubmap(double px, double py, double vx, double vy, int charge){

    if(!is_initialized) initialize();

    double pt = sqrt(px*px+py*py);
    double phi = atan2(py,px);
    double RR = 100 * pt / (0.3 * 3.8);
    if(charge<0) RR = -RR;

    bool isok1, isok2;
    unsigned int u=0;
    for(unsigned int l=0; l<6; ++l){
      double phi1 = prop(phi, RR, vx, vy, Rlayer[l], isok1);
      double phi2 = prop(phi, RR, vx, vy, Rlayer[l]+DRlayer[l], isok2);
      phi2 = asin(sin(phi2 - phi1));
      double bend = phi2 * Rlayer[l];
      if(isok1 && isok2 && bend_cut[l]-fabs(bend)>0 ){
	//trigger stub
	u |= (1<<l);
      }
    }
    return u;
  }

 private:
   //geometry for the stub finding
   double Rlayer[6]={23, 36, 51, 68, 88, 108};
   double DRlayer[6] = {0.26, 0.16, 0.16, 0.18, 0.18, 0.18};
   double pt_cut = 2; //hits are out if stub pt>pt_cut GeV

   bool is_initialized = false;
   double bend_cut[6];

};
